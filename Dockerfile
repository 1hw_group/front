#FROM node:13
#WORKDIR /app

#COPY package.json .
#RUN npm install
#COPY . .
#EXPOSE 3000
#CMD 

FROM node:13-alpine3.11 as builder
WORKDIR /app/
COPY ["package.json", "package-lock.json", "./"]
RUN ["npm", "install"]
COPY ["./", "./"]
RUN ["npm", "run", "build"]

FROM node:13-alpine3.11
WORKDIR /app/
COPY --from=builder /app/ ./
ENTRYPOINT npm start
EXPOSE 3000